import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({ selector: '[gma-width-DropDownList]' })
export class AdjustWidthDirective {
    constructor(private el: ElementRef, private renderer: Renderer) {
       
       
     }
	 ngOnInit() {
		 this.changeWidth();
	 }
    
     @HostListener('change')  changeWidth() {
		
		var selectedOption= this.el.nativeElement.value;
	
		
		var fontFamily= window.getComputedStyle(this.el.nativeElement).getPropertyValue("font-family");
		var fontSize= window.getComputedStyle(this.el.nativeElement).getPropertyValue("font-size");

		//Using the text-width package to calculate the width of a text
		var width = require('text-width');
		var widthText = width(selectedOption, {
								size: fontSize,
								family:fontFamily	
		});
	
		//30 is the size of the down arrow of the select box 
		widthText=widthText+30;
		
		
		var w=(widthText.toString()).concat("px");
		
        this.renderer.setStyle(this.el.nativeElement, 'width', w);
    }

    
}