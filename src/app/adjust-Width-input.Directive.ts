import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({ selector: '[gma-width-input]' })
export class AdjustWidthInputDirective {
    constructor(private el: ElementRef, private renderer: Renderer) {
       
       
    }
	
    
     @HostListener('change')  changeWidth() {
		 
		var inputValue= this.el.nativeElement.value;
		//if the input value is empty set the input width to 700px
		if(inputValue.toString()==""){
			this.renderer.setStyle(this.el.nativeElement, 'width', "700px");
		}
		
		else{
			
			var fontFamily= window.getComputedStyle(this.el.nativeElement).getPropertyValue("font-family");
			var fontSize= window.getComputedStyle(this.el.nativeElement).getPropertyValue("font-size")

			var width = require('text-width');
			var widthText = width(inputValue, {
									size: fontSize,
									family:fontFamily	
			});
			
		
			widthText=widthText+50;
			var str2=(widthText.toString()).concat("px");
			
			this.renderer.setStyle(this.el.nativeElement, 'width', str2);
	   
		
		}
	 }
	 
	
    
}