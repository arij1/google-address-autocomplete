import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { AdjustWidthDirective } from './adjust-width.Directive'; 
import { AdjustWidthInputDirective } from './adjust-width-input.Directive'; 
@NgModule({
  declarations: [
    AppComponent, AdjustWidthDirective,AdjustWidthInputDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
	AgmCoreModule.forRoot({
      apiKey: "AIzaSyBY6_xvuT9kGNVULKlZggMUyIjIfdbxFfc",
      libraries: ["places"]
    }),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
 
  bootstrap: [AppComponent]
})
export class AppModule { }
