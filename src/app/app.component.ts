import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { ElementRef, ViewChild } from '@angular/core';

import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  //variables
  
  addressForm: FormGroup;
 
  
  public searchControl: FormControl;
  public showResult: boolean;
  
  
  
  
  
  
  
  
   @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
   
  ) { this.showResult = false;}
    
	onClick(value: string) {
    alert(value);
  }
  
  

 // form initialised
 ngOnInit() {
    this.addressForm = new FormGroup({
      'adressData': new FormGroup({
        'street_address': new FormControl(null, []),
		'search': new FormControl(null, []),
        'locality': new FormControl(null, []),
		'region' : new FormControl(null, []),
		'postal_code':new FormControl(null, []),
		'country_name': new FormControl(null, [])		
      }),
     
    });
	
	
	
	
	
	

    //create search FormControl
    this.searchControl = new FormControl();

    
	
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
		
      let autocomplete = new google.maps.places.Autocomplete( <HTMLInputElement>(document.getElementById('search')),
      {types: ['geocode']});
	  
	  
      autocomplete.addListener("place_changed", () => {
		  
		 
		  
		   // Get the place details from the autocomplete object.
				let place: google.maps.places.PlaceResult = autocomplete.getPlace();
				var comp0:string[] = new Array() ;
				var comp:string[] = new Array() ;
				 
					
					//splitting the street address
					var stLoc0=place["adr_address"].split("street-address\">",2);
					if (stLoc0.length==2){
						var stLoc1=stLoc0[1].split("</span>",2);
						(<HTMLInputElement>document.getElementById('street_address')).value = stLoc1[0];
						
					}
					else (<HTMLInputElement>document.getElementById('street_address')).value = "";
					
					
					//splitting the postal code
					var postalCode0=place["adr_address"].split("postal-code\">",2);
					if (postalCode0.length==2){
						var postalCode1=postalCode0[1].split("</span>",2);
						(<HTMLInputElement>document.getElementById('postal_code')).value = postalCode1[0];
						
					}
					else (<HTMLInputElement>document.getElementById('postal_code')).value = "";
					
					
					
					//splitting the locality
					var locality0=place["adr_address"].split("locality\">",2);
					if (locality0.length==2){
						var locality1=locality0[1].split("</span>",2);
						(<HTMLInputElement>document.getElementById('locality')).value = locality1[0];
						
					}
					else (<HTMLInputElement>document.getElementById('locality')).value = "";
					
					
					
					//splitting the region
					var region0=place["adr_address"].split("region\">",2);
					if (region0.length==2){
						var region1=region0[1].split("</span>",2);
						(<HTMLInputElement>document.getElementById('region')).value = region1[0];
						
					}
					else(<HTMLInputElement>document.getElementById('region')).value = "";
					
					
					
					//splitting the country_name
					var country0=place["adr_address"].split("country-name\">",2);
					if (country0.length==2){
						var country1=country0[1].split("</span>",2);
						(<HTMLInputElement>document.getElementById('country_name')).value = country1[0];
						
					}
					else (<HTMLInputElement>document.getElementById('country_name')).value = "";
					
					
					
				
				  
					  
				
		  
		  }); 
      });
    
  }
  
  ShowResult() {
        this.showResult = true;
    }
  



  

}
